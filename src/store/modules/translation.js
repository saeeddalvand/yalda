import * as mutationTypes from "../mutation-types";
import config from "@/config/index";

import { createI18n } from "@/i18n/index";

const i18n = createI18n();

export const state = config.locale;

var stateInLocal = JSON.parse(localStorage.getItem("trans"));
const createTransLocalStorage = (currentState) => {
  localStorage.removeItem("trans");
  localStorage.setItem("trans", JSON.stringify(currentState));
  stateInLocal = currentState;
};

const setStateMathLocal = () => {
  if (stateInLocal == null || stateInLocal == undefined) {
    createTransLocalStorage(state);
  }
  state.locale = stateInLocal.locale;
};
setStateMathLocal();
export const getters = {
  locale: () => state.locale,
};

export const mutations = {
  [mutationTypes.SET_LOCALE]: (state, payload) => {
    state.locale = payload !== undefined ? payload : "en";
    i18n.loadLanguageAsync(state.locale);

    if (stateInLocal !== null) {
      stateInLocal.locale = state.locale;
      createTransLocalStorage(stateInLocal);
    }
    if (stateInLocal == null) {
      createTransLocalStorage(state);
    }
  },
};

export const actions = {
  setLocale: ({ commit }, payload) => {
    commit(mutationTypes.SET_LOCALE, payload);
   
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
