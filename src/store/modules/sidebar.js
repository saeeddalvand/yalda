import * as mutationTypes from "../mutation-types";
import config from "@/config/index";

export const state = config.navigations.sidebar;

const defaultActiveMenuStyles = config.navigations.defaultActiveMenuStyles;

var stateInLocal = JSON.parse(localStorage.getItem("sidbar"));
const createSidbarLocalStorage = (currentState) => {
  localStorage.removeItem("sidbar");
  localStorage.setItem("sidbar", JSON.stringify(currentState));
  stateInLocal = currentState;
};

export const getters = {
  isVisibleSideNav: (state) => state.show,
  isMinSideNav: (state) => state.miniVariant,
  activeItemStyle: (state) => state.activeMenuStyle,
  isSidenavPostionRight: (state) => state.isRight,
  isClipped: (state) => state.clipped,
};

const setStateMathLocal = () => {
  if (stateInLocal == null || stateInLocal == undefined) {
    createSidbarLocalStorage(state);
  }

  state.show = stateInLocal.show;
  state.miniVariant = stateInLocal.miniVariant;
  state.activeMenuStyle = stateInLocal.activeMenuStyle;
  state.padless = stateInLocal.padless;
  state.isRight = stateInLocal.isRight;
  state.clipped = stateInLocal.clipped;
};
setStateMathLocal();

export const mutations = {
  [mutationTypes.SET_SIDENAV_VISIBILITY]: (state, payload) => {
    state.show = payload !== undefined ? payload : !state.show;
    if (stateInLocal !== null && payload != undefined) {
      stateInLocal.show = payload;
      createSidbarLocalStorage(stateInLocal);
    }
    if (stateInLocal == null) {
      createSidbarLocalStorage(state);
    }
  },
  [mutationTypes.SET_SIDENAV_MINI_VARIANT]: (state, payload) => {
    state.miniVariant = payload !== undefined ? payload : !state.miniVariant;
    if (stateInLocal !== null && payload != undefined) {
      stateInLocal.miniVariant = state.miniVariant;
      createSidbarLocalStorage(stateInLocal);
    }

    if (stateInLocal == null) {
      createSidbarLocalStorage(state);
    }
  },
  [mutationTypes.SET_SIDENAV_ACTIVE_MENU_STYLE]: (state, payload) => {
    state.activeMenuStyle = !payload
      ? {
          ...defaultActiveMenuStyles,
        }
      : {
          ...defaultActiveMenuStyles,
          [payload]: true,
        };

    if (stateInLocal !== null && payload != undefined) {
      stateInLocal.activeMenuStyle = state.activeMenuStyle;
      createSidbarLocalStorage(stateInLocal);
    }

    if (stateInLocal == null) {
      createSidbarLocalStorage(state);
    }
  },
  [mutationTypes.SET_SIDENAV_POSITION_TO_RIGHT]: (state, payload) => {
    state.isRight = payload !== undefined ? payload : !state.isRight;

    if (stateInLocal !== null) {
      stateInLocal.isRight = state.isRight;
      createSidbarLocalStorage(stateInLocal);
    }

    if (stateInLocal == null) {
      createSidbarLocalStorage(state);
    }
  },
};

export const actions = {
  setSidenavVisibility: ({ commit }, payload) => {
    commit(mutationTypes.SET_SIDENAV_VISIBILITY, payload);
  },
  setMiniVariant: ({ commit }, payload) => {
    commit(mutationTypes.SET_SIDENAV_MINI_VARIANT, payload);
  },
  setActiveSidenavStyle: ({ commit }, payload) => {
    commit(mutationTypes.SET_SIDENAV_ACTIVE_MENU_STYLE, payload);
  },
  setSidenavPositionRight: ({ commit }, payload) => {
     commit(mutationTypes.SET_SIDENAV_POSITION_TO_RIGHT, payload);
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
