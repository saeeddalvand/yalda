import * as mutationTypes from "../mutation-types";
import config from "@/config/index";

export const state = config.navigations.footer;

//var readFromLocal = true;
var stateInLocal = JSON.parse(localStorage.getItem("footer"));
const createFooterLocalStorage = (currentState) => {
  localStorage.removeItem("footer");
  localStorage.setItem("footer", JSON.stringify(currentState));
  stateInLocal = JSON.parse(localStorage.getItem("footer"));
};

export const getters = {
  isVisibleFooter: (state) => state.show,
  isAppFooter: (state) => state.app,
  isFixedFooter: (state) => state.fixed,
  isAbsoluteFooter: (state) => state.absolute,
  isInsetFooter: (state) => state.inset,
  isPadlessFooter: (state) => state.padless,
};

const setStateMathLocal = () => {
  if (stateInLocal == null || stateInLocal == undefined) {
    createFooterLocalStorage(state);
  }

  state.show = stateInLocal.show;
  state.app = stateInLocal.app;
  state.inset = stateInLocal.inset;
  state.padless = stateInLocal.padless;
  state.fixed = stateInLocal.fixed;
  state.absolute = stateInLocal.absolute;
};
setStateMathLocal();
export const mutations = {
  [mutationTypes.SET_FOOTER_VISIBILITY]: (state, payload) => {
    state.show = payload !== undefined ? payload : !state.show;

    if (stateInLocal !== null && payload != undefined) {
      stateInLocal.show = payload;
      createFooterLocalStorage(stateInLocal);
      return;
    }
    if (stateInLocal == null && payload) {
      createFooterLocalStorage(state);
    }
  },
  [mutationTypes.SET_APP_FOOTER]: (state, payload) => {
    state.app = payload !== undefined ? payload : !state.app;
    if (stateInLocal !== null && payload != undefined) {
      stateInLocal.app = state.app;
      createFooterLocalStorage(stateInLocal);
      return;
    }
    if (stateInLocal == null && payload) {
      createFooterLocalStorage(state);
    }
  },
  [mutationTypes.SET_FIXED_FOOTER]: (state, payload) => {
    state.fixed = payload !== undefined ? payload : !state.fixed;
    if (stateInLocal !== null && payload != undefined) {
      stateInLocal.fixed = state.fixed;
      createFooterLocalStorage(stateInLocal);
      return;
    }
    if (stateInLocal == null && payload) {
      createFooterLocalStorage(state);
    }
  },
  [mutationTypes.SET_INSET_FOOTER]: (state, payload) => {
    state.inset = payload !== undefined ? payload : !state.inset;
    if (stateInLocal !== null && payload != undefined) {
      stateInLocal.inset = state.inset;
      createFooterLocalStorage(stateInLocal);
      return;
    }
    if (stateInLocal == null && payload) {
      createFooterLocalStorage(state);
    }
  },
  [mutationTypes.SET_ABSOLUTE_FOOTER]: (state, payload) => {
    state.absolute = payload !== undefined ? payload : !state.absolute;
    if (stateInLocal !== null && payload != undefined) {
      stateInLocal.absolute = state.absolute;
      createFooterLocalStorage(stateInLocal);
      return;
    }
    if (stateInLocal == null && payload) {
      createFooterLocalStorage(state);
    }
  },
  [mutationTypes.SET_PADLESS_FOOTER]: (state, payload) => {
    state.padless = payload !== undefined ? payload : !state.padless;
    if (stateInLocal !== null && payload != undefined) {
      stateInLocal.padless = state.padless;
      createFooterLocalStorage(stateInLocal);
      return;
    }
    if (stateInLocal == null && payload) {
      createFooterLocalStorage(state);
    }
  },
};

export const actions = {
  setFooterVisibility: ({ commit }, payload) => {
    commit(mutationTypes.SET_FOOTER_VISIBILITY, payload);
  },
  setAppFooter: ({ commit }, payload) => {
    commit(mutationTypes.SET_APP_FOOTER, payload);
  },
  setFixedFooter: ({ commit }, payload) => {
    commit(mutationTypes.SET_FIXED_FOOTER, payload);
  },
  setInsetFooter: ({ commit }, payload) => {
    commit(mutationTypes.SET_INSET_FOOTER, payload);
  },
  setAbsoluteFooter: ({ commit }, payload) => {
    commit(mutationTypes.SET_ABSOLUTE_FOOTER, payload);
  },
  setPadlessFooter: ({ commit }, payload) => {
    commit(mutationTypes.SET_PADLESS_FOOTER, payload);
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
