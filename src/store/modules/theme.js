import * as mutationTypes from "../mutation-types";
import config from "@/config/index";

export const state = config.theme;

var stateInLocal = JSON.parse(localStorage.getItem("theme"));
const createThemeLocalStorage = (currentState) => {
  localStorage.removeItem("theme");
  localStorage.setItem("theme", JSON.stringify(currentState));
  stateInLocal = currentState;
};

const setStateMathLocal = () => {
  if (stateInLocal == null || stateInLocal == undefined) {
    createThemeLocalStorage(state);
  }

  state.primary = stateInLocal.primary;
  state.secondary = stateInLocal.secondary;
  state.header = stateInLocal.header;
  state.footer = stateInLocal.footer;
  state.dark = stateInLocal.dark;
  state.rtl = stateInLocal.rtl;
  state.primaryBgText = stateInLocal.primaryBgText;
  state.secondaryBgText = stateInLocal.secondaryBgText;
  state.semidark = stateInLocal.semidark;
};
setStateMathLocal();

export const getters = {
  primary: (state) => state.primary,
  secondary: (state) => state.secondary,
  header: (state) => state.header,
  footer: (state) => state.footer,
  dark: (state) => state.dark,
  rtl: (state) => state.rtl,
  primaryBgText: (state) => state.primaryBgText,
  secondaryBgText: (state) => state.secondaryBgText,
  semidark: (state) => state.semidark,
};

export const mutations = {
  [mutationTypes.SET_PRIMARY_COLOR]: (state, payload) => {
    state.primary = payload.color;
    state.primaryBgText = payload.bgText;
    if (stateInLocal !== null) {
      stateInLocal.primary = payload.color;
      stateInLocal.primaryBgText = payload.bgText;
      createThemeLocalStorage(stateInLocal);
      return;
    }
    if (stateInLocal == null && payload) {
      createThemeLocalStorage(state);
    }
  },
  [mutationTypes.SET_SECONDARY_COLOR]: (state, payload) => {
    state.secondary = payload.color;
    state.secondaryBgText = payload.bgText;

      if (stateInLocal !== null) {
      stateInLocal.secondary = payload.color;
      stateInLocal.secondaryBgText = payload.bgText;
      createThemeLocalStorage(stateInLocal);
      return;
    }
    if (stateInLocal == null) {
      createThemeLocalStorage(state);
    }
  },
  [mutationTypes.SET_HEADER_COLOR]: (state, payload) => {
    state.header = payload;
    if (stateInLocal !== null) {
      stateInLocal.header = payload;
      createThemeLocalStorage(stateInLocal);
      return;
    }
    if (stateInLocal == null) {
      createThemeLocalStorage(state);
    }
  },
  [mutationTypes.SET_FOOTER_COLOR]: (state, payload) => {
    state.footer = payload;
    if (stateInLocal !== null) {
      stateInLocal.footer = payload;
      createThemeLocalStorage(stateInLocal);
      return;
    }
    if (stateInLocal == null) {
      createThemeLocalStorage(state);
    }
  },
  [mutationTypes.SET_DARK_MODE]: (state, payload) => {
    state.semidark = false;
    state.dark = payload !== undefined ? payload : !state.dark;

    if (stateInLocal !== null) {
      stateInLocal.semidark = false;
      stateInLocal.dark=state.dark;
      createThemeLocalStorage(stateInLocal);
      return;
    }
    if (stateInLocal == null) {
      createThemeLocalStorage(state);
    }

  },
  [mutationTypes.SET_SEMI_DARK_MODE]: (state, payload) => {
    state.semidark = payload !== undefined ? payload : !state.semidark;

    if (stateInLocal !== null) {
      stateInLocal.semidark = state.semidark;
      createThemeLocalStorage(stateInLocal);
      return;
    }
    if (stateInLocal == null) {
      createThemeLocalStorage(state);
    }


  },
  [mutationTypes.SET_RTL]: (state, payload) => {
    state.rtl = payload !== undefined ? payload : !state.rtl;
    if (stateInLocal !== null) {
      stateInLocal.rtl = state.rtl;
      createThemeLocalStorage(stateInLocal);
      return;
    }
    if (stateInLocal == null) {
      createThemeLocalStorage(state);
    }
  },
};

export const actions = {
  setPrimaryColor: ({ commit }, payload) => {
    commit(mutationTypes.SET_PRIMARY_COLOR, payload);
  },
  setSecondaryColor: ({ commit }, payload) => {
    commit(mutationTypes.SET_SECONDARY_COLOR, payload);
  },
  setHeaderColor: ({ commit }, payload) => {
    commit(mutationTypes.SET_HEADER_COLOR, payload);
  },
  setFooterColor: ({ commit }, payload) => {
    commit(mutationTypes.SET_FOOTER_COLOR, payload);
  },
  setDarkTheme: ({ commit }, payload) => {
    commit(mutationTypes.SET_DARK_MODE, payload);
  },
  setSemiDarkTheme: ({ commit }, payload) => {
    commit(mutationTypes.SET_SEMI_DARK_MODE, payload);
  },
  setRtl: ({ commit }, payload) => {
    commit(mutationTypes.SET_RTL, payload);
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
