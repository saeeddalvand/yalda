import * as mutationTypes from "../mutation-types";
import config from "@/config/index";

export const state = { ...config.navigations.header };

export const getters = {
  isVisibleHeader: (state) => state.show,
  isClippedOver: (state) => state.clippedOver,
  isDense: (state) => state.dense,
  isProminent: (state) => state.prominent,
  isHideOnScroll: (state) => state.hideOnScroll,
  isShrinkOnScroll: (state) => state.shrinkOnScroll,
  isFloating: (state) => state.floating,
};

var stateInLocal = JSON.parse(localStorage.getItem("header"));
const createHeaderLocalStorage = (currentState) => {
  localStorage.removeItem("header");
  localStorage.setItem("header", JSON.stringify(currentState));
  stateInLocal = JSON.parse(localStorage.getItem("header"));
};

const setStateMathLocal = () => {
  if (stateInLocal == null || stateInLocal == undefined) {
    createHeaderLocalStorage(state);
  }

  state.show = stateInLocal.show;
  state.clippedOver = stateInLocal.clippedOver;
  state.dense = stateInLocal.dense;
  state.prominent = stateInLocal.prominent;
  state.hideOnScroll = stateInLocal.hideOnScroll;
  state.shrinkOnScroll = stateInLocal.shrinkOnScroll;
  state.floating = stateInLocal.floating;
};
setStateMathLocal();

export const mutations = {
  [mutationTypes.SET_HEADER_VISIBILITY]: (state, payload) => {
    state.show = payload !== undefined ? payload : !state.show;
    if (stateInLocal !== null && payload != undefined) {
      stateInLocal.show = payload;
      createHeaderLocalStorage(stateInLocal);
    }
 
    if (stateInLocal == null) {
      createHeaderLocalStorage(state);
    }
  },
  [mutationTypes.SET_HEADER_CLIPPED_OVER]: (state, payload) => {
    state.clippedOver = payload !== undefined ? payload : !state.clippedOver;

    if (stateInLocal !== null && payload != undefined) {
      stateInLocal.clippedOver = state.clippedOver;
      createHeaderLocalStorage(stateInLocal);
    }
  
    if (stateInLocal == null) {
      createHeaderLocalStorage(state);
    }


  },
  [mutationTypes.UPDATE_HEADER_SETTING]: (state, payload) => {
    state = Object.assign(state, payload);
    if (stateInLocal !== null && payload != undefined) {
      stateInLocal = state;
      createHeaderLocalStorage(stateInLocal);
    }
  
    if (stateInLocal == null) {
      createHeaderLocalStorage(state);
    }
  },
};

export const actions = {
  setHeaderVisibility: ({ commit }, payload) => {
    commit(mutationTypes.SET_HEADER_VISIBILITY, payload);
  },
  setHeaderClippedOver: ({ commit }, payload) => {
    commit(mutationTypes.SET_HEADER_CLIPPED_OVER, payload);
  },
  updateHeaderSettings: ({ commit }, payload) => {
    commit(mutationTypes.UPDATE_HEADER_SETTING, payload);
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
