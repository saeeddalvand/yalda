import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import './sass/main.scss'
import { createI18n } from '@/i18n/index'
import axios from 'axios'
Vue.prototype.$http = axios

const i18n = createI18n(store.state.translation.locale).i18n
Vue.config.productionTip = false

import vSelect from 'vue-select'
Vue.component('vue-select', vSelect)

import VuePersianDatetimePicker from 'vue-persian-datetime-picker'
Vue.component('date-picker', VuePersianDatetimePicker)

export const app = new Vue({
	router,
	store,
	vuetify,
	i18n,
	render: h => h(App),
}).$mount('#app')

// https:talkhabi.github.io/vue-persian-datetime-picker/#/
