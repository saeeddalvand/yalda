// eslint-disable-next-line no-unused-vars
const data = [
	{
		id: 1,
		companyName: 'Frozen Yogurt',
		registerNumber: 159,
		fat: 6.0,
		carbs: 24,
		protein: 4.0,
		iron: '1%',
	},
	{
		id: 2,
		companyName: 'Frozen cream sandwich',
		registerNumber: 237,
		fat: 9.0,
		carbs: 37,
		protein: 4.3,
		iron: '1%',
	},
	{
		id: 3,
		companyName: 'Eclair',
		registerNumber: 262,
		fat: 16.0,
		carbs: 23,
		protein: 6.0,
		iron: '7%',
	},
	{
		id: 4,
		companyName: 'Cupcake',
		registerNumber: 305,
		fat: 3.7,
		carbs: 67,
		protein: 4.3,
		iron: '8%',
	},
	{
		id: 5,
		companyName: 'Gingerbread',
		registerNumber: 356,
		fat: 16.0,
		carbs: 49,
		protein: 3.9,
		iron: '16%',
	},
	{
		id: 6,
		companyName: 'Jelly bean',
		registerNumber: 375,
		fat: 0.0,
		carbs: 94,
		protein: 0.0,
		iron: '0%',
	},
	{
		id: 7,
		companyName: 'Lollipop',
		registerNumber: 392,
		fat: 0.2,
		carbs: 98,
		protein: 0,
		iron: '2%',
	},
	{
		id: 8,
		companyName: 'Honeycomb',
		registerNumber: 408,
		fat: 3.2,
		carbs: 87,
		protein: 6.5,
		iron: '45%',
	},
	{
		id: 9,
		companyName: 'Donut',
		registerNumber: 452,
		fat: 25.0,
		carbs: 51,
		protein: 4.9,
		iron: '22%',
	},
	{
		id: 10,
		companyName: 'KitKat',
		registerNumber: 518,
		fat: 26.0,
		carbs: 65,
		protein: 7,
		iron: '6%',
	},
]
export function GetLegal({
	// eslint-disable-next-line no-unused-vars
	page,
	// eslint-disable-next-line no-unused-vars
	itemsPerPage,
	// eslint-disable-next-line no-unused-vars
	companyName = null,
	// eslint-disable-next-line no-unused-vars
	registerNumber = null,
	// eslint-disable-next-line no-unused-vars
	businessCode = null,
	// eslint-disable-next-line no-unused-vars
	nationalCode = null,
}) {
	var filtereditems = data.filter(item => {
		return (
			(companyName !== null && companyName !== ''
				? item.companyName.includes(companyName)
				: true) &&
			(registerNumber !== null && registerNumber !== ''
				? item.registerNumber == registerNumber
				: true)
		)
	})

	return {
		items: filtereditems.slice((page - 1) * itemsPerPage, page * itemsPerPage),
		total: filtereditems.length,
	}
}
