// eslint-disable-next-line no-unused-vars
const data = [
	{
		id: 1,
		systemyCode: '00101235124',
		expert: 'سعید دلوند',
		fileSerial: '3',
		contractNumber: '500000',
		fileType: '2',
		contractType: 'تماس ',

	},
	{
		id: 2,
		systemyCode: '1111222233',
		expert: 'مهدی محمدی',
		fileSerial: '2154',
		contractNumber: '101545',
		fileType: '2',
		contractType: 'هرچی',

	},
	{
		id: 3,
		systemyCode: '55555',
		expert: 'علی محمدی',
		fileSerial: '443',
		contractNumber: '57800000',
		fileType: '45',
		contractType: 'مزایده ',

	},
]
export function GetFilesList({ page, itemsPerPage, filters }) {
	var filtereditems = data.filter(item => {
		return filters['systemyCode'] !== null && filters['systemyCode'] !== ''
			? item.systemyCode.includes(filters['systemyCode'])
			: true
	})

	return {
		items: filtereditems.slice((page - 1) * itemsPerPage, page * itemsPerPage),
		total: filtereditems.length,
	}
}
