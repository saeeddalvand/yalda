// eslint-disable-next-line no-unused-vars
const data = [
	{
		followTypeId: 1,
		followResultId: 2,
		userRoleWorkGroupId: 0,
		personPhoneId: '3fa85f64-5717-4562-b3fc-2c963f66afa6',
		description: 'string',
		personId: '3fa85f64-5717-4562-b3fc-2c963f66afa6',
		createDate: '2021-02-15T15:46:46.726Z',
		createDatePersian: 'string',
		userSaveId: 0,
		efficientFollow: true,
		agreementType: 0,
		agreementMoney: 0,
		agreementDatePersian: 'string',
		agreementDate: '2021-02-15T15:46:46.726Z',
		agreementDescription: 'string',
		agreementPersonId: '3fa85f64-5717-4562-b3fc-2c963f66afa6',
		agreementResult: 0,
		agreementDateResultPersian: '1400/01/02 ',
		agreementDateResult: '2021-02-15T15:46:46.726Z',
		nextFollowDatePersian: '1399/5/5',
		followTypeTitle: 'حضوری',
		followResultTitle: 'پرداخت 2 قسط',
		nextFollowDate: '2021-02-15T15:46:46.726Z',
		caseIdList: ['3fa85f64-5717-4562-b3fc-2c963f66afa6'],
		id: '3fa85f64-5717-4562-b3fc-2c963f66afa1',
	},
	{
		followTypeId: 0,
		followResultId: 0,
		userRoleWorkGroupId: 0,
		personPhoneId: '3fa85f64-5717-4562-b3fc-2c963f66afa6',
		description: 'قرار خیلی خوبی بود قرار شد کلی پول واریز کنه ',
		personId: '3fa85f64-5717-4562-b3fc-2c963f66afa6',
		createDate: '2021-02-15T15:46:46.726Z',
		createDatePersian: '1399/10/10',
		userSaveId: 0,
		efficientFollow: true,
		agreementType: 0,
		agreementMoney: 0,
		agreementDatePersian: '1399/01/10',
		agreementDate: '2021-02-15T15:46:46.726Z',
		agreementDescription: 'قرار مفیدی بود و....',
		agreementPersonId: '3fa85f64-5717-4562-b3fc-2c963f66afa6',
		agreementResult: 0,
		agreementDateResultPersian: '1399/11/11',
		agreementDateResult: '2021-02-15T15:46:46.726Z',
		nextFollowDatePersian: 'string',
		followTypeTitle: 'تلفنی',
		followResultTitle: 'پرداخت اقساط',
		nextFollowDate: '2021-02-15T15:46:46.726Z',
		caseIdList: ['3fa85f64-5717-4562-b3fc-2c963f66afa6'],
		id: '3fa85f64-5717-4562-b3fc-2c963f66afa2',
	},

]
export function GetFollowsList ( { page, itemsPerPage, filters } ) {
	const filtereditems = data;
	console.log( { filters } );
	// var filtereditems = data.filter(item => {
	// 	return filters['systemyCode'] !== null && filters['systemyCode'] !== ''
	// 		? item.systemyCode.includes(filters['systemyCode'])
	// 		: true
	// })
	// filtereditems = filtereditems.filter(item => {
	// 	return filters['debtorFullName'] !== null &&
	// 		filters['debtorFullName'] !== ''
	// 		? item.debtorFullName.includes(filters['debtorFullName'])
	// 		: true
	// })
	return {
		items: filtereditems.slice((page - 1) * itemsPerPage, page * itemsPerPage),
		total: filtereditems.length,
	}
}
