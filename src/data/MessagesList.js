// eslint-disable-next-line no-unused-vars
const data = [
	{
		id: 1,
		systemyCode: '00101235124',
		messageType: 'یادآوری',
		debtorFullName: 'رضا بیگ مرادی',
		number: '09122125142',
		sendDate: '1398/10/10',
		status: 'نمیدونم',
		delivery: 'بله',
	},
	{
		id: 2,
		systemyCode: '22121444',
		messageType: 'یادآوری',
		debtorFullName: 'رضا بیگ مرادی',
		number: '09364662514',
		sendDate: '1399/11/05',
		status: 'عالی',
		delivery: 'خیر',
	},
	{
		id: 3,
		systemyCode: '221214442',
		messageType: 'یادآوری',
		debtorFullName: 'علی محمدی',
		number: '09364662514ی',
		sendDate: '1399/11/15',
		status: 'عالی',
		delivery: 'خیر',
	},
]
export function GetMessagesList({ page, itemsPerPage, filters }) {
					var filtereditems = data.filter(item => {
						return filters['systemyCode'] !== null &&
							filters['systemyCode'] !== ''
							? item.systemyCode.includes(filters['systemyCode'])
							: true
					})

					return {
						items: filtereditems.slice(
							(page - 1) * itemsPerPage,
							page * itemsPerPage
						),
						total: filtereditems.length,
					}
				}
