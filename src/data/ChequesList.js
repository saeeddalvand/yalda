// eslint-disable-next-line no-unused-vars
const data = [
	{
		id: 1,
		systemyCode: '00101235124',
		fileSerial: '3',
		contractNumber: '500000',
		fileType: 'x2',
	},
	{
		id: 2,
		systemyCode: '22121444',
		fileSerial: '4',
		contractNumber: '215451',
		fileType: 'y2',
	},
]
export function GetChequesList({ page, itemsPerPage, filters }) {
					var filtereditems = data.filter(item => {
						return filters['systemyCode'] !== null &&
							filters['systemyCode'] !== ''
							? item.systemyCode.includes(filters['systemyCode'])
							: true
					})

					return {
						items: filtereditems.slice(
							(page - 1) * itemsPerPage,
							page * itemsPerPage
						),
						total: filtereditems.length,
					}
				}
