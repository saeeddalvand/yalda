export default [
	{ icon: 'dashboard', title: 'Home', name: 'Home', i18n: 'Home' },
	{
		title: 'ManagmentFiles',
		group: 'ManagmentFiles',
		icon: 'dashboard',
		i18n: 'ManagmentFiles',
		items: [
			{
				icon: 'person',
				title: 'RegisterIndividualContact',
				name: 'RegisterIndividualContact',
				i18n: 'RegisterIndividualContact',
			},
			{
				icon: 'group_work',
				title: 'RegisterLegalContact',
				name: 'RegisterLegalContact',
				i18n: 'RegisterLegalContact',
			},
			{
				icon: 'insert_drive_file',
				title: 'RegisterDossier',
				name: 'RegisterDossier',
				i18n: 'RegisterDossier',
			},
		],
	},
	{
		title: 'Cartables',
		group: 'Cartables',
		icon: 'dashboard',
		i18n: 'Cartables',
		items: [
			{
				icon: 'person',
				title: 'DailyCartable',
				name: 'DailyCartable',
				i18n: 'DailyCartable',
			},
		],
	},
]
