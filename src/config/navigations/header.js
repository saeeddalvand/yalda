export default {
	show: true,
	clippedOver: false,
	//dense: false,
	dense: true,
	shrinkOnScroll: false,
	hideOnScroll: false,
	prominent: false,
	floating: true,
}
