export const availableLocale = [
  {
    text: "English",
    value: "en",
    // eslint-disable-next-line no-undef
    img: require("@/assets/flags/unitedstates.svg"),
  },
  {
    text: "فارسی",
    value: "fa",
    // eslint-disable-next-line no-undef
    img: require("@/assets/flags/iran.svg"),
  }
];
export default {
  locale: "en",
};
