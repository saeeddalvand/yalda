const allInOne = {
	//appDrawer
	Home: 'خانه',
	ManagmentFiles:'مدیریت پرونده',
	RegFile: ' ثبت پرونده',
	RegisterIndividualContact: 'ثبت اشخاص حقیقی',
	RegisterLegalContact: 'ثبت اشخاص حقوقی',
	RegisterDossier: 'ثبت پرونده',
	Cartables: 'کارتابل',
	DailyCartable:'کارتابل روزانه',

	// city
	Tehran: 'تهران',
	Eslamshahr: 'اسلامشهر',
	Baharestan: 'بهارستان',
	Pakdasht: 'پاکدشت',
	Pardis: 'پردیس',
	Pishva: 'پیشوا',
	Damavand: 'دماوند',
	Shahryar: 'شهریار',
	Shahreghods: 'شهر قدس',
	Borujerd: 'بروجرد',
	Khoramabad: 'خرماباد',
	Azna: 'ازنا',
	Aligudarz: 'الیگودرز',
	Poldokhtar: 'پلدختر',
	Doroud: 'درود',
	Chegeny: 'چگنی',
	Delphan: 'دلفان',
	Selsele: 'سلسله',
	Kuhdasht: 'کوهدشت',
	Lorestan: 'لرستان',
	//
	//RegisterDossier
	DossierFormTitle: 'ثبت شرکت',
	FileNumberInstitute: 'شماره پرونده موسسه',
	ReferralDate: 'تاریخ ارجاع',
	ContractDate: 'تاریخ قرارداد',
	ContractNumber: 'شماره قرارداد',
	ContentType: 'نوع محتوا',
	ContentTypes: {
		ContentType1: 'محتوا نوع 1',
		ContentType2: 'محتوا نوع 2',
	},
	ArchiveNumber: ' شماره بایگانی',
	CustomerNumber: ' شماره مشتری',
	EmployerContractType: 'نوع قرارداد کارفرما',
	EmployerContractTypes: {
		EmployerContractType1: 'قرارداد نوع 1',
		EmployerContractType2: 'قرارداد نوع 2',
	},
	DossierType: 'نوع پرونده',
	DossierTypes: {
		DossierType1: 'نوع 1',
		DossierType2: 'نوع 2',
	},
	ContractSubject: 'موضوع قرارداد',
	ContractSubjects: {
		ContractSubject1: 'موضوع قرارداد 1',
		ContractSubject2: 'موضوع قرارداد 2',
	},
	Bank: 'بانک',
	Banks: {
		Bank1: 'بانک 1',
		Bank2: 'بانک 2',
	},
	Branch: 'شعبه',
	Branches: {
		Branch1: {
			Branch: 'شعبه  1',
			Bank: 'بانک 1',
			Address: 'ادرس 1',
		},
		Branch2: {
			Branch: 'شعبه 2',
			Bank: 'بانک  2',
			Address: 'آدرس 2',
		},
		Branch3: {
			Branch: 'شعبه 3',
			Bank: 'بانک 3',
			Address: 'ادرس  3',
		},
	},
	ContractType: 'نوع  قرارداد',
	ContractTypes: {
		ContractType1: 'قرارداد  نوع 1',
		ContractType2: 'قرارداد   نوع 2',
	},
	CountDays: 'تعداد روز',
	CountClearDays: ' تعداد روز های پاک',
	StartDate: 'تاریخ شروع',
	DeadLineDate: 'تاریخ سر رسید',
	OrginalEmountLoan: 'اصل بدهی وام',
	AllProfitLoan: ' کل سود',
	InstalmentAmount: 'مبلغ قسط',
	InstalmentCount: 'تعداد اقساط',
	ProfitPersent: 'درصد سود',
	LateFeesPredent: 'درصد جریمه',
	Step1: 'مشخصات قرارداد',
	Step2: 'مشخصات مدیون',
	Step3: 'جزییات وام',
	Continue: 'ادامه',
	Back: 'برگشت',
	//
	//RegisterIndividualContact
	IndividualFormTitle: 'ثبت اشخاص حقیقی',
	FirstName: 'نام',
	LastName: 'فامیلی',
	Register: {
		Success: '  با موفقیت ثبت گردید',
		Faild: ' ثبت شخص با خطا مواجه شد',
	},
	Gender: 'جنسیت',
	Genders: {
		Female: 'زن',
		Male: 'مرد',
	},
	FatherName: 'نام پدر',
	SerialNumber: 'شماره شناسنامه',
	IssuedPlace: 'محل صدور',
	BurnedPlace: 'محل تولد',
	NationalCode: 'کد ملی',
	BirthDate: 'تاریخ تولد',
	IssueDate: 'محل صدور',
	Email: 'ایمیل',
	Site: 'سایت',
	Boss: 'کارفرمای قرارداد وصول',
	//
	//RegisterLegalContact
	LegalFormTitle: 'ثبت اشخاص حقوقی',

	LegalTypes: {
		LegalType1: '  سهامی عام',
		LegalType2: ' سهامی خاص',
	},
	CompanyName: 'عنوان شرکت',
	RegisterDate: 'تاریخ ثبت',
	RegisterNumber: ' شماره ثبت',
	RegisterPlace: 'محل ثبت',
	BusinessCode: 'کد اقتصادی',
	NationalLegalCode: 'کد ملی',
	BusinessSubject: 'موضوع فعالیت',
	LegalEmail: 'ایمیل',
	LegalSite: 'سایت',
	LegalBoss: 'کارفرمای قرارداد وصول',
	LegalType: 'نوع شرکت',

	// Validations
	Required: ' پر کردن این فیلد ضروریست',
	MaxError: ' حداکثر تعداد کراکتر بایدبرابر باشد با: ',
	MinError: ' حداقل تعداد کراکتر باید برابر باشد با:',
	//

}

export default allInOne
