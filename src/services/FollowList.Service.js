/* eslint-disable no-unused-vars */
import axios from 'axios'
import { baseConfig } from './config'

export const FollowListService = {
	getAll,
	save,
	update,
}

async function getAll ( { sortBy, sortDesc, page, itemsPerPage, filters, personId }) {
	var result = await axios.get(
		baseConfig.baseUri +
			'Follow/GetAllFollowListByPersonId?personId=' +
			personId
	)
	console.log('result in service.js',{result});
	return result
}

async function save({ title }) {
	const newsType = {
		title: title,
	}
	const config = { headers: { 'Content-Type': 'application/json' } }

	var result = await this.$http.post(
		baseConfig.baseUri + 'posttype/AddNewPostType',
		newsType,
		config
	)

	return result
}

async function update({ id, title }) {
	const newsType = {
		id: id,
		title: title,
	}
	const config = {
		headers: {
			'Content-Type': 'application/json',
			'Access-Control-Allow-Origin': '*',
		},
	}
	var result = await this.$http.put(
		baseConfig.baseUri + 'posttype/UpdatePostType/' + id,
		newsType,
		config
	)
	return result
}
