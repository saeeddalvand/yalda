/* eslint-disable no-unused-vars */
import axios from 'axios'
import { baseConfig } from './config'

export const TypeOfNewsService = {
	getAll,
	save,
	update,
}

async function getAll({ page, itemsPerPage }) {
	var result = await axios.get(
		baseConfig.baseUri + 'posttype/GetAllNewsTypesForGrid',
		{
			params: {
				page: page,
				itemsPerPage: itemsPerPage,
			},
		}
	)
	return result
}

async function save({ title }) {
	const newsType = {
		title: title,
	}
	const config = { headers: { 'Content-Type': 'application/json' } }

	var result = await axios.post(
		baseConfig.baseUri + 'posttype/AddNewPostType',
		newsType,
		config
	)

	return result
}

async function update({ id, title }) {
	const newsType = {
		id: id,
		title: title,
	}
	const config = {
		headers: {
			'Content-Type': 'application/json',
			'Access-Control-Allow-Origin': '*',
		},
	}
	var result = await axios.put(
		baseConfig.baseUri + 'posttype/UpdatePostType/' + id,
		newsType,
		config
	)
	return result
}
