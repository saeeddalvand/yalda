/* eslint-disable no-unused-vars */
import { BehaviorSubject } from 'rxjs'
import axios from 'axios'
import { baseConfig } from './config'
const currentUserSubject = new BehaviorSubject()

export const authenticationService = {
	login,
	currentUser: currentUserSubject.asObservable(),
	get currentUserValue() {
		return currentUserSubject.value
	},
}
// async function login(loginInfo) {
//   const res = await axios.post(baseConfig.baseUri+"login", loginInfo)
//   return res;
// }
async function login ( loginInfo ) {
	// eslint-disable-next-line no-debugger
	debugger;
	console.log( { loginInfo });
	const res = await axios.post( baseConfig.baseUri + 'user/token', loginInfo )
	console.log({res})
	const success = { status: 200 };
//	const faild = { status: 400 }
	return success;
}
// after
// function logout() {
//   // remove user from local storage to log user out
//   // localStorage.removeItem("currentUser");
//   currentUserSubject.next(null);
// }
