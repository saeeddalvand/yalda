import axios from "axios";
import { baseConfig } from "./config";

export const roleService = {
  getRolesForSelect
};

async function getRolesForSelect() {
     var result = await axios.get(baseConfig.baseUri + "role/getRolesForSelect");
    return result.data;
}
