/* eslint-disable no-unused-vars */
import axios from 'axios'
// import { config } from 'vue/types/umd'
import { baseConfig } from './config'

export const DailyCartableService = {
	getAll,
	//save,
	//update,
}

async function getAll({ sortBy, sortDesc, page, itemsPerPage, filters }) {
	// // eslint-disable-next-line no-debugger
	// debugger
	// //axios.baseURL = 'http://192.168.1.145:1414/api/v1/'
	axios.defaults.headers.common['Authorization'] =
		'Bearer ' +
		'eyJhbGciOiJBMTI4S1ciLCJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwidHlwIjoiSldUIn0.dG9tw0dEtChGpL3qTfu0fThSxjgykbJ7l9k5ELqa9CZfT2AxB9-t4g.GpaDRynAJj-wB88kITTNwA.A4RB98NbA27DcfLSaPLNERbbBEpv1fDIWTlt4qi6fJBcYs8RoGDxEWPUq6z8bkp2Fy_jsfZ4or42qdyN0MTeoT3GSqchk_LqKrBgPJ012w0mRWyv9YpUsyfjChZrYZG_XOYmqzBq0jOBengwagpTHnyhX_f1JIZtJEb1XytMxUuXvvLDt4T0m25nhlOuLK_Afnk_d7AC257AuSbbyLFqHVRlqEkd390Vlz4S2QF8GVcC2ya-0cKNAIZtigYCB8HuWOCPloqoyqC_89zj_XFyKOXu-Fa1WDAt6MLvIsfjZ9DpmTGZ11aNiyQT7HC1h2-2OqzMZJctUHuiTAIQyKy0elbHXPgMXoTRIBTYB9ZKx3lUW_OCG2rRYVRuYV0pfCbuyNdEaPpIh6Vj5o38pmI7nhfwbjLVjXQYRqakovGg8QhcZKBw7mFwaiPXCfZVZam190n5UiIhulA8ZuQa_CgHDLk2eVb-ZpvVwiEQ-S4f0LcjnLEcfiU7Y-G7F2FdWRaVwJRTv4aJ_mdpXC9mIOcGTlQKFxEQNShjcJpzOuN9oKeqgnDyn4rq6enV9xKdEuZNEiam_APWZukPdryEHgAYXbObIUult5U6yp3yatnFnZ5QkPhfh_vQ1QLN2KH-1bJwqG2CCYin5spkl_YRm5CP8uX9XTZ5JjIdPveKwO8hUf4.jQpJojiaAeGeOOwWOsFErg'
	axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*'
	axios.defaults.headers.common['Accept'] = '*/*'
	var result = await axios.get(
		baseConfig.baseUri + 'CaseCartable/GetPersonListCartableUser'
	)
	// var result = await axios.get(baseConfig.baseUri + 'bank')
	// console.log('result=>', result)
	// return result

	//var result = await axios.get(baseConfig.baseUri + 'bank')
	console.log('result in service.js', { result })
	return result
}
