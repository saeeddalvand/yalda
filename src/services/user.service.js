/* eslint-disable no-unused-vars */
import axios from 'axios'
import { baseConfig } from './config'
import { handleResponse, requestOptions } from '../_helpers'

export const userService = {
	getAll,
	getById,
	save,
	update,
}

async function getAll({
	page,
	itemsPerPage,
	userName = null,
	firstName = null,
	lastName = null,
	mobile = null,
}) {
	var result = await axios.get(baseConfig.baseUri + 'user/GetAllUserForGrid', {
		params: {
			page: page,
			itemsPerPage: itemsPerPage,
			userName: userName,
			firstName: firstName,
			lastName: lastName,
			mobile: mobile,
		},
	})
	return result
}

function getById(id) {
	return fetch(`${baseConfig.baseUri}/users/${id}`, requestOptions.get()).then(
		handleResponse
	)
}

async function save({
	userName,
	password,
	firstName,
	lastName,
	email,
	active,
	address,
	roleIds,
	birthDate,
	mobile,
}) {
	if (birthDate != null && birthDate != undefined && birthDate != '') {
		birthDate = birthDate.trim()
	}
	const userInfo = {
		userName: userName,
		password: password,
		firstName: firstName,
		lastName: lastName,
		email: email,
		active: active,
		address: address,

		roleIds: roleIds,
		birthDate: birthDate,
		mobile: mobile,
	}
	const config = { headers: { 'Content-Type': 'application/json' } }

	var result = await axios.post(
		baseConfig.baseUri + 'user/AddNewUser',
		userInfo,
		config
	)

	return result
}

async function update({
	id,
	userName,
	password,
	firstName,
	lastName,
	email,
	active,
	address,
	roleIds,
	birthDate,
	intId,
	mobile,
}) {
	if (birthDate != null && birthDate != undefined && birthDate != '') {
		birthDate = birthDate.trim()
	}
	const userInfo = {
		id: id,
		userName: userName,
		password: password,
		firstName: firstName,
		lastName: lastName,
		email: email,
		active: active,
		address: address,
		intId: intId,
		roleIds: roleIds,
		birthDate: birthDate,
		mobile: mobile,
	}
	const config = {
		headers: {
			'Content-Type': 'application/json',
			'Access-Control-Allow-Origin': '*',
		},
	}
	var result = await axios.put(
		baseConfig.baseUri + 'user/update/' + id,
		userInfo,
		config
	)
	return result
}
