export {
	DailyCartableHeaders,
	DailyCartableFilter,
	FollowListHeader,
	FollowListFilter,
	checkSelectedRow,
}

const DailyCartableHeaders = [
	{
		text: 'کد مدیون',
		align: 'start',
		sortable: false,
		value: 'receiverPersonId',
		hasFilter: true,
	},

	{ text: 'مدیون', value: 'receiverPersonFullName', hasFilter: true },
	{ text: 'تعداد پرونده', value: 'caseCount', hasFilter: true },
	{ text: 'مبلغ بدهی', value: 'sumDebtMoney', hasFilter: true },
	{
		text: 'تعداد اقساط معوق',
		value: 'sumInstallmentDelayCount',
		hasFilter: true,
	},
	{ text: 'آخرین تاریخ پیگیری', value: 'lastFollowDate', hasFilter: true },
	{ text: 'آخرین تاریخ واریزی', value: 'lastPaymentDate', hasFilter: true },
	{ text: 'کد ملی مدیون', value: 'nationalCode', hasFilter: true },
	{ text: 'موبایل', value: 'mobile', hasFilter: true },
	{ text: 'تلفن', value: 'tel', hasFilter: true },
]

const checkSelectedRow = function(selectedDebtor) {
	if (selectedDebtor[0] == undefined || selectedDebtor[0] == null) {
		alert('لطفا یک مدیون را انتخاب نمایید سپس اقدام نمایید')
		return false
	}
	return true
}

const FollowListHeader = [
	{
		text: 'کد سیستمی',
		align: 'start',
		sortable: false,
		value: 'id',
		hasFilter: true,
	},
	{ text: 'نوع پیگیری', value: 'followTypeTitle', hasFilter: true },
	{ text: 'نتیجه پیگیری', value: 'followResultTitle', hasFilter: true },
	{
		text: 'تاریخ پیگیری بعدی',
		value: 'nextFollowDatePersian',
		hasFilter: true,
	},
	{ text: 'تاریخ قرار', value: 'agreementDateResultPersian', hasFilter: true },
]

const FollowListFilter = {
	id: '',
	followTypeTitle: '',
	followResultTitle: '',
	nextFollowDatePersian: '',
	agreementDateResultPersian: '',
}
const DailyCartableFilter = {
	debtorCode: '',
	debtorFullName: '',
	filesCount: '',
	amountDebt: '',
	debtCount: '',
	lastFollow: '',
	lastDateOfPay: '',
	NationalCode: '',
	mobile: '',
	phone: ''
}