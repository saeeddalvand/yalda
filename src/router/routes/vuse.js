// Authentication Pages

export default [
	{
		params: {
			path: '/home',
			name: 'Home',
		},
		defaultComponentPath: 'Pages/Home',
		navs: true,
	},
	{
		params: {
			path: '/regFile',
			name: 'RegFile',
		},
		defaultComponentPath: 'Pages/RegFile',
		navs: true,
	},
	{
		params: {
			path: '/followsList',
			name: 'Followslist',
		},
		defaultComponentPath: 'Pages/FollowsList',
		navs: true,
	},
	{
		params: {
			path: '*',
			name: 'PageNotFound',
			meta: { layout: 'full' },
		},
		defaultComponentPath: 'Pages/Errors/Error404',
	},
]
