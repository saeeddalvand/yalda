// Authentication Pages

export default [
	{
		params: {
			path: '/registerIndividualContact',
			name: 'ManagmentFiles/RegisterIndividualContact',
		},
		defaultComponentPath: 'Pages/RegisterIndividualContact',
		navs: true,
	},
	{
		params: {
			path: '/registerLegalContact',
			name: 'ManagmentFiles/RegisterLegalContact',
		},
		defaultComponentPath: 'Pages/RegisterLegalContact',
		navs: true,
	},
	{
		params: {
			path: '/registerDossier',
			name: 'ManagmentFiles/RegisterDossier',
		},
		defaultComponentPath: 'Pages/RegisterDossier',
		navs: true,
	},
	{
		params: {
			path: '/DailyCartable',
			name: 'Cartables/DailyCartable',
		},

		defaultComponentPath: 'Pages/DailyCartable',
		navs: true,
	},

	{
		params: {
			path: '/login',
			name: 'Login',
		},
		defaultComponentPath: 'Pages/Authentication/Login/Login',
		navs: true,
	},
]
