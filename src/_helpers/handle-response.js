import { authenticationService } from "../services/index";

export function handleResponse(response) {
  if (response.status == 200) return response;
  if (!response.status == 200) {
    if ([401, 403].indexOf(response.status) !== -1) {
      // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
      authenticationService.logout();
      location.reload(true);
    }
  }
  // return response.then(text => {
  //   const data = text && JSON.parse(text);
  //   if (!response.status) {
  //     if ([401, 403].indexOf(response.status) !== -1) {
  //       // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
  //       authenticationService.logout();
  //       location.reload(true);
  //     }

  //     const error = (data && data.message) || response.statusText;
  //     return Promise.reject(error);
  //   }

  //   return data;
  // });
}
